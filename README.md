# Template App to test Gitlab CI with CakePHP 3

[![pipeline status](https://gitlab.com/koffisani/test-ci/badges/master/pipeline.svg)](https://gitlab.com/koffisani/test-ci/commits/master)
[![coverage report](https://gitlab.com/koffisani/test-ci/badges/master/coverage.svg)](https://gitlab.com/koffisani/test-ci/commits/master)


## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Install and configure Git on your PC.
3. Clone this project
4. Update dependencies : `composer update`

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Configuration

Copy `config/app.default.php` to `config/app.php`.

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your application.
